package in.traveltale.common.utility;

public interface Constants {

	String USER_ROLE_USER = "USER";
	
	String USER_ROLE_ADMIN = "ADMIN";
}
