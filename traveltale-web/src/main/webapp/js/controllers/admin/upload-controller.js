travelTale.controller('destinationController',['$scope','uploadService','comService', function($scope,uploadService,comService){

var Destination = function()
{
  this.name = null;
	this.category = null;
	this.type = null;
	this.creator = null;
	this.pincode = null;
  this.state = null;
  this.country = null;
  this.city = null;
}
//$scope.image = null;

$scope.dest = new Destination();
$scope.createDestination = function()
{
  console.log("destination object",$scope.dest);
  console.log("image : ",$scope.image);
    $scope.dest.name = $scope.dest.name.toUpperCase();
    uploadService.create({methodName:'create'},$scope.dest,function(response){
      if(response.status === 'OK')
      {
        comService.showSuccess("Destination "+$scope.dest.name+" created successfully. You can add photos for this destination now.");
        console.log("Successfully created.");
      }
      else{
        comService.showError("There was some error while creating the destination "+$scope.dest.name+".");
        console.error("Got an error.");
      }
  });
};

}]).directive("unique",['$q','$timeout','uploadService',function($q, $timeout,uploadService){
  return {
      require: 'ngModel',
      link: function(scope, elm, attrs, ctrl) {

        var destinations = [];

        uploadService.get({methodName : 'getAllNames'},{},function(response){
          if(response.status === 'OK')
          {
            console.log("success in getting names");
            destinations = response.data;
            console.log("all dests",destinations);

          }
          else{
            console.error("The list of destinations was not found.");
          }
        })

        ctrl.$asyncValidators.unique = function(modelValue, viewValue) {

          if (ctrl.$isEmpty(modelValue)) {
            // consider empty model valid
            return $q.when();
          }

          var def = $q.defer();
          $timeout(function() {
            // Mock a delayed response
            if (destinations.indexOf(modelValue.toUpperCase()) === -1) {
              // The username is available
              def.resolve();
            } else {
              def.reject();
            }

          }, 2000);

          return def.promise;
        };
      }
    };
}]);
