'use strict';
travelTale.controller('UserController', ['$scope','$http','comService','userService', function($scope,$http,comService,userService) {
	userService.getUser({methodName:'get',username:comService.getCurrentUser()},{},function(response){
		$scope.loginUser = response.data;
	});

	var User = function(){
			this.mobileNumber = null;
			this.emailId = null;
			this.password = null;
			this.name = null;
			this.address = null;
	};

	$scope.toggleModal = function(bool)
	{
		if(bool == undefined){
			$scope.showForm = !$scope.showForm;
		}
		else{
			$scope.showForm = bool;
		}
	};

		$scope.user = new User();
		$scope.showForm=false;

	    $scope.show = function(){
	    	$scope.user = new User();
	    	//$scope.userCreationForm.$setPristine();
	    	$scope.showForm=!$scope.showForm;
	    	};

	    	$scope.create = function(user,userCreationForm)
	    	{
							userService.create({methodName:'create'},$scope.user,function(response)
								{
									console.log("response outside",response);
									if(response.status === 'OK')
								{
									var msg = "The user has been created successfully. Please click Login button to sign in.";
									comService.showSuccess(msg);
								}
								else{
									comService.showError("There was error while user creation");
								}
								})
						$scope.showForm=false;
						userCreationForm.$setSubmitted(false);
						userCreationForm.$setPristine();
						userCreationForm.$setUntouched();
				};
	    /**
	     * For updating user data
	     */
	    $scope.updateUser = function()
	    {
	    	userService.update({methodName:'update'},$scope.loginUser,function(response)
    				{
    			console.log("response outside",response);
    			if(response.status === 'OK')
				{
					comService.showSuccess("Your details have been updated.");
				}
				else{
					comService.showError("There was error while user creation");
				}
			})
	    }
	}]);
