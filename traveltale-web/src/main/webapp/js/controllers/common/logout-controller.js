travelTale.controller('LogoutController', ['$scope','$http','$rootScope','$location','comService', function($scope,$http,$rootScope,$location,comService) {
	$scope.logout = function() {
		  $http.post('logout', {}).success(function() {
		    $rootScope.authenticated = false;
		    comService.showSuccess("You have logged out successfully.");
		    $location.path("/");
		  }).error(function(data) {
		    $rootScope.authenticated = false;
		    comService.showSuccess("You have logged out successfully.");
		  });
	}
}]);