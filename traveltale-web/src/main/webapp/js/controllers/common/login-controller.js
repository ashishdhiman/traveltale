/**
 * This popup is used for logging in to the application
 */
travelTale.controller('LoginController', ['$rootScope','$scope','$http','$location','comService', function($rootScope, $scope, $http, $location, comService) {

	var authenticate = function(credentials, callback) {
	var headers = credentials ? {authorization : "Basic "
        + btoa(credentials.username + ":" + credentials.password)
    } : {};

    $http.get('user', {headers : headers}).success(function(data) {
        if (data.name) {
          $rootScope.authenticated = true;
          $rootScope.sessionUser = data.name;
          $rootScope.$broadcast("authenticated");
        } else {
          $rootScope.authenticated = false;
        }
        callback && callback();
      }).error(function() {
        $rootScope.authenticated = false;
        callback && callback();
      });
	}

	authenticate();
	$scope.credentials = {};
	$scope.login = function() {
	      authenticate($scope.credentials, function() {
	        if ($rootScope.authenticated) {
	          $location.path("/home");
	          comService.showSuccess("You have logged in successfully.")
	          $rootScope.$broadcast("success");
	        } else {
	          $location.path("/login");
	          comService.showError("There Username/Password combination does not exist. Please try again")
	        }
	      });
	      $scope.showModal = false;
	  };


	$scope.showModal = false;
    $scope.toggleModal = function(bool)
    	{
    		if(bool == undefined){
    			$scope.showModal = !$scope.showModal;
    		}
    		else{
    			$scope.showModal = bool;
    		}
    		$scope.credentials = {};
    	};
}]);
