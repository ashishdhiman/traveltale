'use strict';

var travelTale = angular.module('main-app',['ngRoute','ngResource']);
travelTale.config([ '$routeProvider', '$httpProvider','$locationProvider', function($routeProvider, $httpProvider, $locationProvider) {

				$routeProvider.when('/', {
								redirectTo  : '/home',
                                  	})
                               	.when('/login', {
                               		templateUrl : 'views/login-pop.html',
                               	})
                               	.when('/home',{
                               		templateUrl : 'views/home-page.html',
                              		controller : 'HomeController'
                               	})
                               	.when('/weather', {
                               		templateUrl : 'views/features/weather.html',
                               	})
                               	.when('/places', {
                               		templateUrl : 'views/features/places.html',
                               	})
                               	.when('/useraccount', {
                               		templateUrl : 'views/user/user-account.html',
                               	})
																.when('/fileupload', {
                               		templateUrl : 'views/admin/create-destinations.html',
																	controller : 'destinationController'
                               	}).
                               	otherwise('/');

					$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
					$locationProvider.html5Mode(true);

				} ]).run(['$http','$rootScope',function($http,$routeScope){
					$http.get('/static/json/tabs.json').then(function(res){
          $routeScope.tabs = res.data;
					console.log("Tabs inside run",$routeScope.tabs);
        });
				}]).directive('mainHeader', function() {
                                    	  return {
                                  		    restrict: 'A',
                                  		    templateUrl: 'views/header.html',
                                  		    //controller: 'HeaderController'
                                  		  };
                                  		}).directive('mainFooter', function() {
                                      	  return {
                                    		    restrict: 'A',
                                    		    templateUrl: 'views/footer.html'
                                    		  };
                                    		});
