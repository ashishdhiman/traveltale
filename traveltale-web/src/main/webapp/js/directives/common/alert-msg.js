travelTale.directive('selfClosingAlert',[ '$timeout','$rootScope', function($timeout,$rootScope){
  return {
    scope: {
      message: '@',
    },
    link: link,
    restrict: 'E',
    replace: true,
    template: '<div class="{{typeClass}}" ng-show="isVisible">'+
	'<button type="button" class="close" ng-click="isVisible=!isVisible" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
	'<strong>{{message}}</strong></div>'
  }

  function link(scope, element, attrs){

	  $rootScope.$on("success",function(){
			  scope.isVisible = true;
			  scope.typeClass = "alert alert-success alert-dismissible";
			    $timeout(function (){
			    		scope.isVisible = false;
			    		$rootScope.alertMsg = "";
			            }, 5000);
	  });

	  $rootScope.$on("error",function(){
		  scope.isVisible = true;
		  scope.typeClass = "alert alert-warning alert-dismissible";
		    $timeout(function (){
		    		scope.isVisible = false;
		    		$rootScope.alertMsg = "";
		            }, 5000);
	  });
	  }
}]);
