travelTale.service('comService', ['$rootScope',
  function($rootScope) {
	
	this.showSuccess = function(msg)
	{
		$rootScope.alertMsg = msg;
		$rootScope.$broadcast("success");
	};
	
	this.showError = function(msg)
	{
		$rootScope.alertMsg = msg;
		$rootScope.$broadcast("error");
	};
	
	this.getCurrentUser = function()
	{
		return $rootScope.sessionUser;
	}
	
	this.removeCurrentUser = function()
	{
		$rootScope.sessionUser = null;
	}
  }]);