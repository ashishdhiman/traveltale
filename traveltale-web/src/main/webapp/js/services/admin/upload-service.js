travelTale.service('uploadService',['$resource',function($resource){

return $resource('/rest/destinations/:methodName',{},{
  create : {
    method : 'POST',
    isArray:false
  },
  getAll : {
    method : 'GET',
    params : {
    },
    isArray:false
  }
});
}]);
