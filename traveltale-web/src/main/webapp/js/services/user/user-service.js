travelTale.service('userService', ['$resource', function($resource){
    return $resource('/rest/user/:methodName', {}, {
                    getUser: {
                        method: 'GET',
                        params: {
                        	username: '@username'
                        		},
                        isArray: false
                    },
                    create:{
                    	method: 'POST',
                        isArray: false
                    },
                    update:{
                    	method: 'POST',
                        isArray: false
                    }
                });
}]);