<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<html ng-app="main-app">
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="/" />
<style type="text/css">
[ng\:cloak], [ng-cloak], .ng-cloak {
	display: none !important;
}
</style>
<!-- <base href="/"> -->
<title>Travel </title>

	<link rel="stylesheet" href="../css/bootstrap/bootstrap.css">
	<link rel="stylesheet" href="../css/bootstrap/bootstrap-theme.css">
	<link rel="stylesheet" href="../css/bootstrap/bootstrap.vertical-tabs.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="../css/app.css">
	<link rel="stylesheet" href="../css/bootstrap/3-col-portfolio.css">
</head>
<body>
	<div main-header></div>
	<div ng-view id="wrap">
	</div>
	<div main-footer></div>

	<!-- Libraries/Vendors -->
	<script type="text/javascript" src="../js/libs/jquery/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="../js/libs/angular/angular.js"></script>
	<script type="text/javascript" src="../js/libs/angular-animate/angular-animate.js"></script>
	<script type="text/javascript" src="../js/libs/angular-resource/angular-resource.js"></script>
	<script type="text/javascript" src="../js/libs/angular-route/angular-route.min.js"></script>
	<script type="text/javascript" src="../js/libs/angular-resource/angular-resource.js"></script>
	<script type="text/javascript" src="../js/libs/bootstrap/bootstrap.js"></script>

	<!-- Resources -->
	<script src="../js/app.js"></script>

	<!-- Services -->
	<script type="text/javascript" src="../js/services/common/common-service.js"></script>
	<script type="text/javascript" src="../js/services/user/user-service.js"></script>
	<script type="text/javascript" src="../js/services/admin/upload-service.js"></script>

	<!-- Controllers -->
	<script src="../js/controllers/user/user-controller.js" type="text/javascript"></script>
	<script src="../js/controllers/home/home-controller.js" type="text/javascript"></script>
	<script src="../js/controllers/common/login-controller.js" type="text/javascript"></script>
	<script src="../js/controllers/common/logout-controller.js" type="text/javascript"></script>
	<script src="../js/controllers/admin/upload-controller.js" type="text/javascript"></script>

	<!-- Directives -->
	<script src="../js/directives/common/pop-up-directive.js" type="text/javascript"></script>
	<script src="../js/directives/common/alert-msg.js" type="text/javascript"></script>

</body>
</html>
