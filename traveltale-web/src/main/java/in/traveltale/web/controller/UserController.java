package in.traveltale.web.controller;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import in.traveltale.common.utility.CommonUtility;
import in.traveltale.common.utility.Constants;
import in.traveltale.persistence.entities.User;
import in.traveltale.service.base.UserRoleService;
import in.traveltale.service.base.UserService;
import in.traveltale.web.mapping.factory.EntityFactory;
import in.traveltale.web.mapping.factory.ValueObjectFactory;
import in.traveltale.web.value.objects.ResponseJSON;
import in.traveltale.web.value.objects.UserVO;

/**
 * 
 * @author Ashish
 *
 */
@Component
@Path(value = "/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private UserRoleService userRoleService;
	
	private static PasswordEncoder passwordEncoder = new BCryptPasswordEncoder(); 
	
	/**
	 * This method is user for creation of user
	 * @param u
	 */
	@POST
	@Produces(value = MediaType.APPLICATION_JSON)
	@Consumes(value = MediaType.APPLICATION_JSON)
	@Path("/create")
	public ResponseJSON createUser(UserVO user)
	{
		System.out.println("user creation");
		ResponseJSON response = new ResponseJSON();
		try{
			User u = EntityFactory.getUserFromUserVO(user);
			u.setPassword(passwordEncoder.encode(user.getPassword()));
			u.setUserRole(userRoleService.findUserRole(Constants.USER_ROLE_USER));
			userService.createUser(u);
		}
		catch(Exception e)
		{
			response.setStatus("KO");
			response.setError(e.getMessage());
		}
		return response;
	}
	
	/**
	 * This method is user for creation of user
	 * @param u
	 */
	@POST
	@Produces(value = MediaType.APPLICATION_JSON)
	@Consumes(value = MediaType.APPLICATION_JSON)
	@Path("/update")
	public ResponseJSON updateUser(UserVO user)
	{
		ResponseJSON response = new ResponseJSON();
		try{
			User entity = userService.findUserByEmailId(user.getEmailId());
			userService.updateUser(EntityFactory.setNewFields(user, entity));
		}
		catch(Exception e)
		{
			response.setStatus("KO");
			response.setError(e.getMessage());
		}
		return response;
	}
	
	@GET
	@Produces(value = MediaType.APPLICATION_JSON)
	@Consumes(value = MediaType.APPLICATION_JSON)
	@Path("/get")
	  public ResponseJSON getUser(@QueryParam(value = "username")String username) {
		User user = null;
	    if(CommonUtility.isLong(username))
	    {
	    	user = userService.findUserByMobileNumber(Long.valueOf(username));
	    }
	    else{
	    	user = userService.findUserByEmailId(username);
	    }
	    UserVO uvo = ValueObjectFactory.getUserVOFromUser(user);
	    ResponseJSON response = new ResponseJSON();
	    response.setData(uvo);
	    return response;
	  }
}
