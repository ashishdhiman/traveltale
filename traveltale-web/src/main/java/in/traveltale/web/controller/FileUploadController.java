package in.traveltale.web.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import in.traveltale.common.utility.CommonUtility;
import in.traveltale.persistence.entities.FileUpload;
import in.traveltale.service.base.FileUploadService;
import in.traveltale.web.mapping.factory.EntityFactory;
import in.traveltale.web.value.objects.FileVO;
import in.traveltale.web.value.objects.ResponseJSON;

@Path("/fileUpload")
@Component
public class FileUploadController {
	
	@Autowired
	private FileUploadService fileUploadService;
	
	@POST
	@Produces(value = MediaType.APPLICATION_JSON)
	@Consumes(value = MediaType.APPLICATION_JSON)
	@Path("/upload")
	public ResponseJSON uploadFile(FileVO file)
	{
		ResponseJSON resp = new ResponseJSON();
		FileUpload fileUpload = EntityFactory.getFileEntity(file);
		FileUpload output = fileUploadService.createNewFile(fileUpload);
		if(CommonUtility.isNull(output))
		{
			resp.setStatus("KO");
		}
		return resp;
	}
	

}
