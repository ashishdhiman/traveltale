package in.traveltale.web.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import in.traveltale.common.utility.CommonUtility;
import in.traveltale.persistence.entities.Destination;
import in.traveltale.service.base.DestinationManagerService;
import in.traveltale.web.mapping.factory.EntityFactory;
import in.traveltale.web.mapping.factory.ValueObjectFactory;
import in.traveltale.web.value.objects.DestinationVO;
import in.traveltale.web.value.objects.ResponseJSON;

@Path(value="/destinations")
@Component
public class DestinationController {
	
	
	@Autowired
	private DestinationManagerService destinationManager;
	
	@POST
	@Path("/create")
	@Consumes(value = MediaType.APPLICATION_JSON)
	@Produces(value = MediaType.APPLICATION_JSON)
	public ResponseJSON createDestination(DestinationVO destination)
	{
		System.out.println("dest creation");
		ResponseJSON response = new ResponseJSON();
		Destination destEntity = EntityFactory.getDestinationEntity(destination);
		Destination output = destinationManager.createDestination(destEntity);
		if(CommonUtility.isNull(output))
		{
			response.setStatus("KO");
		}
		else
		{
			response.setData(ValueObjectFactory.getDestinationVO(output));
		}
		return response;
	}
	
	@GET
	@Path("/getAllNames")
	@Consumes(value = MediaType.APPLICATION_JSON)
	@Produces(value = MediaType.APPLICATION_JSON)
	public ResponseJSON getAllNames()
	{
		ResponseJSON response = new ResponseJSON();
		List<String> names = destinationManager.getAllNames();
		response.setData(names);
		return response;
	}
}
