/**
 * 
 */
package in.traveltale.web.controller;

import java.security.Principal;

import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


/**
 * @author hungrymind
 *
 */
@RestController
@PropertySource("classpath:application.properties")
public class LoginController {
	
	
	@RequestMapping(value = {"/","/{[path:[^\\.]*}"})
	public ModelAndView showIndex() { 
		ModelAndView model = new ModelAndView();
		model.setViewName("index"); 
		return model;
	}
	
	@RequestMapping("/user")
	public Principal user(Principal user) {
		return user;
	  }
	
}
