/**
 * 
 */
package in.traveltale.web.ws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

/**
 * @author hungrymind
 *
 */
@Component
@Path("/test")
public class EndPoints {
	
	@GET
	@Path("/test")
	@Produces(value = MediaType.APPLICATION_JSON)
	public String helloWorld() {
		return "Hello World !";
	}

}
