/**
 * 
 */
package in.traveltale.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * @author hungrymind
 *
 */
@Configuration
@ComponentScan(basePackages={"in.traveltale"})
@PropertySource("classpath:application.properties")
public class MvcConfig extends WebMvcConfigurerAdapter{
	@Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
	
	@Bean       
    public InternalResourceViewResolver internalResourceViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/");
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);
        return resolver;
    }
	
	@Override
	 public void addResourceHandlers(ResourceHandlerRegistry registry) {
	  registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	 }
	
	@Bean
	 public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
	  return new PropertySourcesPlaceholderConfigurer();
	 }
	
	@Bean(name = "messageSource")
	 public ReloadableResourceBundleMessageSource messageSourceResolver() {
	  ReloadableResourceBundleMessageSource resource = new ReloadableResourceBundleMessageSource();
	  resource.setBasename("classpath:commands");
	  resource.setDefaultEncoding("UTF-8");	  
	  return resource;

	 }
}