/**
 * 
 */
package in.traveltale.web.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import in.traveltale.web.controller.DestinationController;
import in.traveltale.web.controller.FileUploadController;
import in.traveltale.web.controller.UserController;
import in.traveltale.web.ws.EndPoints;

/**
 * @author hungrymind
 *
 */
@Configuration
@ApplicationPath("/rest")
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
//		ObjectMapper objectMapper = new ObjectMapper();
//		register(new JacksonJsonProvider(objectMapper));
		register(EndPoints.class);
		register(UserController.class);
		register(DestinationController.class);
		register(FileUploadController.class);
	}
}