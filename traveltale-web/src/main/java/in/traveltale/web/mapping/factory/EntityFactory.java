package in.traveltale.web.mapping.factory;


import in.traveltale.common.utility.CommonUtility;
import in.traveltale.persistence.entities.Address;
import in.traveltale.persistence.entities.Destination;
import in.traveltale.persistence.entities.FileUpload;
import in.traveltale.persistence.entities.User;
import in.traveltale.web.value.objects.DestinationVO;
import in.traveltale.web.value.objects.FileVO;
import in.traveltale.web.value.objects.UserVO;

public class EntityFactory {
	
	public static User  getUserFromUserVO(UserVO u)
	{
		User user = new User();
		if(CommonUtility.isNotNull(u))
		{
			user.setAddress(u.getAddress());
			user.setEmailId(u.getEmailId());
			user.setMobileNumber(u.getMobileNumber());
			user.setName(u.getName());
		}
		return user;
	}
	
	/**
	 * This method is used for update mode mainly
	 * @param source
	 * @param destination
	 * @return
	 */
	public static User  setNewFields(UserVO source,User destination)
	{
		if(CommonUtility.isNotNull(source) && CommonUtility.isNotNull(destination))
		{
			if(CommonUtility.isNotNull(source.getAddress()))
				destination.setAddress(source.getAddress());
			if(CommonUtility.isNotNull(source.getEmailId()))
				destination.setEmailId(source.getEmailId());
			if(CommonUtility.isNotNull(source.getName()))
				destination.setName(source.getName());
		}
		return destination;
	}
	
	public static FileUpload getFileEntity(FileVO input)
	{
		FileUpload output = new FileUpload();
		if(CommonUtility.isNotNull(input))
		{
		output.setFile(input.getFile());
		output.setCategory(input.getCategory());
		output.setFileName(input.getFileName());
		output.setFileSize(input.getFileSize());
		output.setFileType(input.getFileType());
		output.setUploader(input.getUploader());
		}
		return output;
	}
	
	public static Destination getDestinationEntity(DestinationVO input)
	{
		Destination output =  new Destination();
		if(CommonUtility.isNotNull(input))
		{
			output.setName(input.getName());
			output.setCategory(input.getCategory());
			output.setCreator(input.getCreator());
			
			//Set address fields
			Address address =  new Address();
			address.setCity(input.getCity());
			address.setCountry(input.getCountry());
			address.setPincode(input.getPincode());
			address.setState(input.getState());
			
			output.setAddress(address);
		}
		return output;
	}

}
