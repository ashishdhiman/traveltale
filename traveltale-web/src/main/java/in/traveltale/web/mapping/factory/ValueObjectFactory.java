package in.traveltale.web.mapping.factory;

import in.traveltale.common.utility.CommonUtility;
import in.traveltale.persistence.entities.Destination;
import in.traveltale.persistence.entities.FileUpload;
import in.traveltale.persistence.entities.User;
import in.traveltale.web.value.objects.DestinationVO;
import in.traveltale.web.value.objects.FileVO;
import in.traveltale.web.value.objects.UserVO;
/**
 * 
 * @author Ashish
 *
 */
public class ValueObjectFactory {
	
	public static UserVO getUserVOFromUser(User u)
	{
		UserVO uvo = new UserVO();
		if(CommonUtility.isNotNull(u))
		{
			uvo.setAddress(u.getAddress());
			uvo.setEmailId(u.getEmailId());
			uvo.setMobileNumber(u.getMobileNumber());
			uvo.setName(u.getName());
			//uvo.setPassword(u.getPassword());
		}
		return uvo;
	}
	
	public static FileVO getFileVO(FileUpload input)
	{
		FileVO output = new FileVO();
		if(CommonUtility.isNotNull(input))
		{
		output.setFile(input.getFile());
		output.setCategory(input.getCategory());
		output.setFileName(input.getFileName());
		output.setFileSize(input.getFileSize());
		output.setFileType(input.getFileType());
		output.setUploader(input.getUploader());
		}
		return output;
	}
	
	public static DestinationVO getDestinationVO(Destination input)
	{
		DestinationVO output = new DestinationVO();
		if(CommonUtility.isNotNull(input))
		{
			output.setName(input.getName());
			output.setCategory(input.getCategory());
			output.setCreator(input.getCreator());
			if(CommonUtility.isNotNull(input.getAddress()))
			{
				output.setCity(input.getAddress().getCity());
				output.setCountry(input.getAddress().getCountry());
				output.setPincode(input.getAddress().getPincode());
				output.setState(input.getAddress().getState());
			}
		}
		return output;
	}
}
