/**
 * 
 */
package in.traveltale.web.value.objects;

/**
 * @author Ashish
 *This is response object which will be used for all the request
 */
public class ResponseJSON {

	/**
	 * Default constructor
	 */
	public ResponseJSON() {
		this.status = "OK";
	}
	
	private String responseCode;
	
	/**
	 * Can have only two values OK and KO
	 */
	private String status;
	
	private Object data;
	
	private String error;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
