package in.traveltale.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

import in.traveltale.web.config.MvcConfig;

/**
 * @author hungrymind
 *
 */
@SpringBootApplication
@Import(MvcConfig.class)
public class TraveltaleWebApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(TraveltaleWebApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(TraveltaleWebApplication.class, args);
	}
}
