/**
 * 
 */
package in.traveltale.persistence.config;

import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author Ashish
 *
 */
@Configuration
@EnableJpaRepositories(basePackages={"in.traveltale"})
@EntityScan(basePackages={"in.traveltale"})
public class PersistenceConfig {

}