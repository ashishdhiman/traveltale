/**
 * 
 */
package in.traveltale.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;


/**
 * @author Ashish
 *
 */

@SuppressWarnings("serial")
@Entity
public class User extends AbstractAuditableEntity{
	
	@NotNull
	@Column(unique=true)
	private Long mobileNumber;
	
	@NotNull
	@Column(unique=true)
	private String emailId;
	
	@NotNull
	private String password;
	
	private String name;
	
	private String address;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_role",referencedColumnName="id", nullable=true)
    private UserRole userRole;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the mobileNumber
	 */
	public Long getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(Long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	@Override
	public String toString() {
		return "User [mobileNumber=" + mobileNumber
				+ ", emailId=" + emailId + ", password=" + password
				+ ", address=" + address + ", activated="
				+ ", userRole=" + userRole + "]";
	}
	
	
	
}
