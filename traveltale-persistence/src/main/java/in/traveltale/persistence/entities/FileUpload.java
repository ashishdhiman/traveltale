package in.traveltale.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * 
 * @author Ashish
 *
 */

@SuppressWarnings("serial")
@Entity
public class FileUpload extends AbstractAuditableEntity{

	@NotNull
	private byte[] file;
	
	@NotNull
	private String fileName;
	
	@NotNull
	private String category;
	
	@NotNull
	private String fileSize;
	
	private String fileType;
	
	private String uploader;
	
	

	public byte[] getFile() {
		return file;
	}



	public void setFile(byte[] file) {
		this.file = file;
	}



	public String getFileName() {
		return fileName;
	}



	public void setFileName(String fileName) {
		this.fileName = fileName;
	}



	public String getCategory() {
		return category;
	}



	public void setCategory(String category) {
		this.category = category;
	}



	public String getFileSize() {
		return fileSize;
	}



	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}



	public String getFileType() {
		return fileType;
	}



	public void setFileType(String fileType) {
		this.fileType = fileType;
	}



	public String getUploader() {
		return uploader;
	}



	public void setUploader(String uploader) {
		this.uploader = uploader;
	}



	@Override
	public String toString() {
		return "User [fileName=" + fileName
				+ ", category=" + category + ", fileSize=" + fileSize
				+ ", fileType=" + fileType + ", activated="
				+ ", uploader=" + uploader + "]";
	}
	
	
}
