package in.traveltale.persistence.entities;

import javax.persistence.Embeddable;

/**
 * 
 * @author Ashish
 *
 */
@Embeddable
public class Address {
	
	private Long pincode;
	
	private String state;
	
	private String country;
	
	private String city;

	public Long getPincode() {
		return pincode;
	}

	public void setPincode(Long pincode) {
		this.pincode = pincode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
}
