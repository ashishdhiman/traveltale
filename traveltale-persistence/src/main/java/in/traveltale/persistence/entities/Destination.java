package in.traveltale.persistence.entities;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 * 
 * @author Ashish
 *
 */

@SuppressWarnings("serial")
@Entity
public class Destination extends AbstractAuditableEntity{

	@NotNull
	private String name;
	
	private String category;
	
	private String kind;
	
	private String creator;
	
	@Embedded
	private Address address;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getType() {
		return kind;
	}

	public void setType(String type) {
		this.kind = type;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
}
