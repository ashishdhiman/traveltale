package in.traveltale.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.traveltale.persistence.entities.FileUpload;

/**
 * @author Ashish
 *
 */

@Repository
@Transactional
public interface FileUploadRepository extends JpaRepository<FileUpload, Long> {
	
}
