package in.traveltale.persistence.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import in.traveltale.persistence.entities.Destination;
/**
 * 
 * @author Ashish
 *
 */
@Repository
@Transactional
public interface DestinationRepository extends JpaRepository<Destination, Long>{
	
	@Query("select d from Destination d")
	List<Destination> getAllDestinations();
	
	@Query("select d from Destination d where d.name = :name")
	Destination getDestinationByName(@Param("name")String name);
	
	@Query("select d from Destination d where d.category = :category")
	List<Destination> getDestinationByCategory(@Param("category")String name);
	
	@Query("select d.name from Destination d")
	List<String> getAllNames();

}
