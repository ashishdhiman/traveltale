/**
 * 
 */
package in.traveltale.persistence.repositories;

import in.traveltale.persistence.entities.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author hungrymind
 *
 */
@Repository
@Transactional
public interface UserRoleRepository extends JpaRepository<UserRole, Long>{
	
	public UserRole findByRole(String userRole);

}