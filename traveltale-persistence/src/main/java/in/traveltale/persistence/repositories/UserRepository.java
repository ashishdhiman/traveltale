/**
 * 
 */
package in.traveltale.persistence.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.traveltale.persistence.entities.User;

/**
 * @author Ashish
 *
 */

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Long>{
	
	@Query("select u from User u")
	public List<User> findUserDetails();
	
	@Query("select u from User u where u.emailId = :emailId")
	public User findByEmailId(@Param("emailId")String emailId);

	@Query("select u from User u where u.mobileNumber = :mobileNumber")
	public User findByMobileNumber(@Param("mobileNumber")Long mobileNumber);
}
