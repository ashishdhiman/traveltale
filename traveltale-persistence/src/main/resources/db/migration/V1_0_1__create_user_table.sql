CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_date` datetime DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) NOT NULL,
  `mobile_number` bigint(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_role` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_email_id` (`email_id`),
  UNIQUE KEY `UK_mobile_number` (`mobile_number`),
  KEY `FK_user_role` (`user_role`),
  CONSTRAINT `FK_user_role` FOREIGN KEY (`user_role`) REFERENCES `user_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;