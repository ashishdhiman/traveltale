package in.traveltale.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.traveltale.persistence.entities.FileUpload;
import in.traveltale.persistence.repositories.FileUploadRepository;
import in.traveltale.service.base.FileUploadService;

@Service
public class FileUploadServiceImpl implements FileUploadService{
	
	@Autowired
	FileUploadRepository fileUploadRepo;

	@Override
	public FileUpload createNewFile(FileUpload file) {
		fileUploadRepo.save(file);
		return file;
	}

}
