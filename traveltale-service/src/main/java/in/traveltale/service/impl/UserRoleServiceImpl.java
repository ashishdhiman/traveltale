/**
 * 
 */
package in.traveltale.service.impl;

import in.traveltale.persistence.entities.UserRole;
import in.traveltale.persistence.repositories.UserRoleRepository;
import in.traveltale.service.base.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author hungrymind
 *
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {
	
	@Autowired
	UserRoleRepository userRoleRepo;

	@Override
	public UserRole findUserRole(String userRole) {
		return userRoleRepo.findByRole(userRole);
	}

	

}