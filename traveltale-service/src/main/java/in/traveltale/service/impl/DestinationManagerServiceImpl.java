package in.traveltale.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.traveltale.persistence.entities.Destination;
import in.traveltale.persistence.repositories.DestinationRepository;
import in.traveltale.service.base.DestinationManagerService;
/**
 * 
 * @author Ashish
 *
 */
@Service
public class DestinationManagerServiceImpl implements DestinationManagerService{

	@Autowired
	DestinationRepository destinationRepo;
	
	@Override
	public Destination createDestination(Destination destination) {
		return destinationRepo.save(destination);
	}

	@Override
	public List<String> getAllNames() {
		// TODO Change to Set
		return destinationRepo.getAllNames();
	}
}
