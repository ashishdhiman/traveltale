package in.traveltale.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import in.traveltale.persistence.entities.User;
import in.traveltale.persistence.repositories.UserRepository;
import in.traveltale.service.base.UserService;

/**
 * @author Ashish
 *
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepo;
	
	@Override
	public User createUser(User user){
		user = userRepo.save(user);
		return user;
	}

	@Override
	public List<User> findUserDetails() {
		return userRepo.findUserDetails();
	}

	@Override
	public User findUserByMobileNumber(Long mobileNumber) {
		return userRepo.findByMobileNumber(mobileNumber);
	}

	@Override
	public User findUserByEmailId(String emailId) {
		return userRepo.findByEmailId(emailId);
	}

	@Override
	public User updateUser(User user) {
		return userRepo.save(user);
	}
}
