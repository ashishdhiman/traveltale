package in.traveltale.service.base;

import org.springframework.stereotype.Service;

import in.traveltale.persistence.entities.FileUpload;

/**
 * 
 * @author Ashish
 *
 */
@Service
public interface FileUploadService {
	
	FileUpload createNewFile(FileUpload file);

}
