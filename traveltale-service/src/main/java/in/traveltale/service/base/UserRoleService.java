/**
 * 
 */
package in.traveltale.service.base;

import in.traveltale.persistence.entities.UserRole;
import org.springframework.stereotype.Service;

/**
 * @author Ashish
 *
 */
@Service
public interface UserRoleService {
	
	public UserRole findUserRole(String userRole);
}
