package in.traveltale.service.base;

import java.util.List;

import org.springframework.stereotype.Service;

import in.traveltale.persistence.entities.User;

/**
 * @author Ashish
 *
 */
@Service
public interface UserService {
	
	public User createUser(User user);	
	public User updateUser(User user);
	public List<User> findUserDetails();
	public User findUserByMobileNumber(Long mobileNumber);
	public User findUserByEmailId(String emailId);
}
