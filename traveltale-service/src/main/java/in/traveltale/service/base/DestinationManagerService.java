package in.traveltale.service.base;

import java.util.List;

import org.springframework.stereotype.Service;

import in.traveltale.persistence.entities.Destination;

/**
 * 
 * @author Ashish
 *
 */
@Service
public interface DestinationManagerService{
	
	Destination createDestination(Destination destination);
	
	List<String> getAllNames();

}
