package in.traveltale.security.service;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import in.traveltale.common.utility.CommonUtility;
import in.traveltale.persistence.entities.User;
import in.traveltale.persistence.entities.UserRole;
import in.traveltale.service.base.UserService;

/**
 * 
 * @author Ashish
 *
 */
@Service("userDetailsService")
public class SecurityUserDetailService implements UserDetailsService{

	@Autowired
	private UserService userService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = null;
		if(CommonUtility.isLong(username))
		{
			user = userService.findUserByMobileNumber(Long.valueOf(username));
		}
		else{
			user = userService.findUserByEmailId(username);
		}
		if(CommonUtility.isNull(user))
		{
			throw new UsernameNotFoundException("User was not found");
		}
		List<GrantedAuthority> grantedAuthList = buildUserAuthorities(user.getUserRole());
		
		return buildUserForAuthentication(user,grantedAuthList);
	}


	private UserDetails buildUserForAuthentication(User user, List<GrantedAuthority> grantedAuthList) {
		return new org.springframework.security.core.userdetails.User(user.getEmailId(),user.getPassword(),true,true,true,true,grantedAuthList);
	}


	private List<GrantedAuthority> buildUserAuthorities(UserRole userRole) {
		Set<GrantedAuthority> authSet = new HashSet<GrantedAuthority>();
		authSet.add(new SimpleGrantedAuthority(userRole.getRole()));
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(authSet);
		return authList;
		
	}
}
